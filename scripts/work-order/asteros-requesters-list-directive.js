(function () {
    angular.module('asterosModule').directive('asterosRequetersList', ['$state', 'astServiceRequestModel', 'relationModel',
        function ($state, astServiceRequestModel, relationModel) {
            return {
                restrict: 'E',
                templateUrl: 'views/work-order/requesters-list-directive.html',
                scope: {
                    ticket: '='
                },
                link: function (scope) {
                    scope.requesters = [];

                    function init() {
                        relationModel.getRelations(scope.ticket.id, EntityVO.TYPE_TASK).then(function (relations) {
                            var srRelation = _.chain(relations)
                                .filter(function (item) {
                                    return item.relationshipType === EntityVO.TYPE_SERVICEREQUEST;
                                })
                                .first()
                                .value();
                            astServiceRequestModel.getRequesters(srRelation.id).then(function (requesters) {
                                scope.requesters = requesters;
                            });
                        });
                    }
                    init();

                    scope.moveToPerson = function(requester){
                        $state.go('person', {id: requester.loginId});
                    }
                }
            }
        }])
})();