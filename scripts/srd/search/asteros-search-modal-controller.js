(function () {
    angular.module('asterosModule').controller('AsterosMultisearchModalController',
        ['$scope', 'astSearchModel', 'applyHandler', 'searchConfig', 'i18nService', 'additionalFilter',
            function ($scope, astSearchModel, applyHandler, searchConfig, i18nService, additionalFilter) {
                $scope.config = {};
                $scope.filterData = {};
                $scope.selectedItems = [];
                $scope.isLoading = false;

                $scope.tableData = {};
                $scope.tableData.items = [];
                $scope.dataType = 'request';
                $scope.tableData.config = {
                    labels: [],
                    fields: []
                };
                $scope.searchConfig = searchConfig;

                $scope.getLocalizedString = function(label){
                    var key = 'asteros.filter.table.' + $scope.dataType + '.' + label.toLowerCase().replace(/\s/g, '');
                    var localized = i18nService.getLocalizedString(key);
                    if (key === localized){
                        localized = $scope.searchConfig.defaultLabel;
                    }
                    return localized;
                };

                function setTableConfig(){
                    var tableConfig = {};
                    tableConfig.dataType = 'request';
                    tableConfig.labels = searchConfig.fieldList.map(function (field) {
                        return $scope.getLocalizedString(field.fieldName);
                    });
                    tableConfig.fields = searchConfig.fieldList;
                    $scope.tableData.config = tableConfig;
                }
                setTableConfig();

                $scope.search = function () {
                    $scope.isLoading = true;
                    var filterData = _.clone($scope.filterData);
                    filterData.questions = additionalFilter.filter;

                    astSearchModel.searchByForm(searchConfig.instanceId, filterData).then(function (records) {
                        $scope.tableData.items = records;
                    }).finally(function(){
                        $scope.isLoading = false;
                    })
                };

                $scope.close = function () {
                    $scope.filterData = [];
                    $scope.filteredRecords = [];
                    $scope.$close();
                };

                $scope.apply = function () {
                    var items = $scope.selectedItems.map(function (item) {
                        return  {
                            id: item[searchConfig.keyField.fieldName],
                            label: item[searchConfig.labelField.fieldName]
                        }
                    });
                    applyHandler(items);
                    $scope.close();
                };

                $scope.selectItem = function (item, isSelected) {
                    if (isSelected) {
                        $scope.selectedItems.push(item);
                        if (!$scope.searchConfig.isMultipleChoise){
                            $scope.apply();
                        }
                    }
                    else {
                        $scope.selectedItems = $scope.selectedItems.filter(function (filterItem) {
                            return filterItem[searchConfig.keyField.fieldName] !== item[searchConfig.keyField.fieldName];
                        })
                    }
                }
            }])
})();