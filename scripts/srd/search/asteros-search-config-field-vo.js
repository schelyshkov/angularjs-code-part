function AstSearchConfigFieldVO(){
    this.fieldName = "";
    this.fieldLabel = "";
}

AstSearchConfigFieldVO.prototype = new AstBaseVO();
AstSearchConfigFieldVO.prototype.constructor = AstSearchConfigFieldVO;

AstSearchConfigFieldVO.prototype.getProps = function(){
    return ['fieldName', 'fieldLabel'];
};