(function(){
    angular.module('asterosModule').service('astSearchService', ['$resource',function($resource){
        var resource = $resource('',
            {},
            {
                searchByForm: {url: '/ux/rest/search/by_question/:id', method: 'GET', isArray: true},
            });

        this.searchByForm = function(form, filter){
            var requestParams = _.clone(filter);
            requestParams.id = form;
            return resource.searchByForm(requestParams).$promise.then(function (response) {
                return response;
            });
        };
    }])
})();