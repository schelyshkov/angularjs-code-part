(function () {
    'use strict';
    angular.module('asterosModule').service('astSearchModel', ['astSearchService', function (astSearchService) {
        this.searchByForm = function (form, filter) {
            var request = {};
            for (var key in filter) {
                request[key] = filter[key];
            }
            return astSearchService.searchByForm(form, request).then(function (response) {
                var items = response[0].items;
                return items.map(function (item) {
                    return item.map;
                });
            });
        };
    }]);
})();