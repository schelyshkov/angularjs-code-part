(function(){
    'use strict';

    angular.module('asterosModule').directive('asterosSimpleField', ['$templateRequest', '$compile', 'ticketTemplateModel', function($templateRequest, $compile, ticketTemplateModel){
        return{
            restrict: 'E',
            replace: true,
            scope:{
                field: '=',
                ticket: '=',
                parent: '='
            },
            controller: function($scope){
                $scope.question = angular.copy($scope.field.config.question);
                $scope.ticket.questionDefinitions.push($scope.question);
                if (!$scope.field.value) {
                    $scope.field.value = new AstFieldValueVO().build({questionId: $scope.field.config.questionId});
                }
                $scope.question.answer = $scope.field.value.value;
                $scope.question.answerLabel = $scope.field.value.label;
                $scope.question.astOldId = $scope.question.id;
                $scope.question.id = 'ID' + _.random(0, 100000, false);
                initValue();

                $scope.$watch('question.answer', function(){
                    $scope.triggerActions($scope.question);
                });

                $scope.triggerActions = function(question){
                    $scope.field.value = new AstFieldValueVO().build({
                            value: question.answer,
                            label: question.answerLabel,
                            questionId: $scope.field.config.questionId
                        });
                };

                $scope.$on('$destroy', function(){
                    $scope.ticket.questionDefinitions = _.filter($scope.ticket.questionDefinitions, function(question){
                        return question.id !== $scope.question.id;
                    });
                });

                function initValue(){
                    if ($scope.question.format === 'DYNAMIC_MENU') {
                        var relatedQuestions = [];
                        $scope.question.options = [];
                        for (var key in $scope.ticket.questionDefinitions){
                            var question = $scope.ticket.questionDefinitions[key];
                            if (question.id !== $scope.parent.value.questionId){
                                relatedQuestions.push(question);
                            } else{
                                var parentQuestion = angular.copy(question);
                                parentQuestion.answer = $scope.parent.value.value;
                                relatedQuestions.push(parentQuestion);
                            }
                        }
                        $scope.relatedQuestions = relatedQuestions;
                    }
                }
            },
            link: function (scope, element) {
                $templateRequest('views/srd/fields/asteros-simple-field-directive.html').then(function(html){
                    var template = angular.element(html);
                    element.append(template);
                    $compile(template)(scope);
                });
            }
        }
    }])
})();