(function () {
    'use strict';

    angular.module('asterosModule').directive('asterosMultisearchField', ['$templateRequest', '$compile', 'astSRDModel', function ($templateRequest, $compile, astSRDModel) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                field: '=',
                ticket: '=',
                isParent: '=',
                parent: '='
            },
            controller: function ($scope) {
                $scope.isSimple = !$scope.field.config.children || !$scope.field.config.children.length;
                $scope.inputValue = null;
                setInputValue();

                $scope.showSearchModal = function (){
                    var parentValue = $scope.parent && $scope.parent.value ? $scope.parent.value.value : null;
                    var additionalFilter = astSRDModel.getAdditionalQuery($scope.field.config, $scope.ticket.questionDefinitions, parentValue);
                    astSRDModel.showSearchModel($scope.field.config, additionalFilter, applyModalHandler);
                };

                function applyModalHandler() {
                    return function (data) {
                        refreshFieldData(data);
                    }
                }

                $scope.removeItem = function (item) {
                    if ($scope.field.value){
                        $scope.field.value = $scope.field.value.filter(function (oldItem) {
                            return item.value !== oldItem.value;
                        });
                    }
                    if ($scope.field.subFields){
                        $scope.field.subFields = $scope.field.subFields.filter(function (oldItem) {
                            return item.value !== oldItem.value.value;
                        });
                    }
                    setInputValue();
                };

                $scope.$on(ASTEROS_EVENTS.SRD.MULTICHOISE_REFRESH, function(e, value){
                    if ($scope.field.config.relatedQuestionIds.indexOf(value.id) > -1){
                        $scope.field.value = [];
                        $scope.field.subFields = [];
                    }
                });

                function refreshFieldData(data) {
                    $scope.field = astSRDModel.formatFieldByData(data, $scope.field);
                    setInputValue();
                }

                function setInputValue(){
                    if ($scope.field.subFields && $scope.field.subFields.length > 0) {
                        $scope.inputValue = _.map($scope.field.subFields, function(){ return 'parent';}).join(':');
                    } else if ($scope.field.value) {
                        if (_.isArray($scope.field.value)) {
                            $scope.inputValue = _.map($scope.field.value, function (item) {
                                return item.value
                            }).join(':');
                        }
                        else {
                            $scope.inputValue = $scope.field.value.value;
                        }
                    } else {
                        $scope.inputValue = null;
                    }
                }
            },
            link: function (scope, element) {
                var templateUrl = '';
                if (scope.isSimple){
                    //if (scope.config.questionDefinition)
                    templateUrl = 'views/srd/fields/asteros-multisearch-field-simple-directive.html';
                }
                else{
                    templateUrl = 'views/srd/fields/asteros-multisearch-filed-directive.html';
                }
                $templateRequest(templateUrl).then(function(html){
                    var template = angular.element(html);
                    element.append(template);
                    $compile(template)(scope);
                });
            }
        }
    }]).directive('asterosMultisearchMaxItemsValidator', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attr, ngModel) {
                var maxItems = +attr['asterosMultisearchMaxItemsValidator'] || 0;
                ngModel.$validators.astMaxItems = function (modelValue, viewValue) {
                    return !!(maxItems < 1 || !modelValue || modelValue && modelValue.split(':').length <= maxItems);
                };
            }
        }
    });
})();