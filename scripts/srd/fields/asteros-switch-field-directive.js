(function(){
    'use strict';

    angular.module('asterosModule').directive('asterosSwitchField', ['$templateRequest', '$compile', function($templateRequest, $compile){
        return {
            restrict: 'E',
            replace: true,
            scope: {
                field: '=',
                ticket: '=',
                parent: '='
            },
            controller: function($scope){

            },
            link: function (scope, element) {
                $templateRequest('views/srd/fields/asteros-switch-field-directive.html').then(function(html){
                    var template = angular.element(html);
                    element.append(template);
                    $compile(template)(scope);
                });
            }
        }
    }])
})();