(function(){
    angular.module('asterosModule').directive('asterosCompositeField', ['$templateRequest', '$compile',function($templateRequest, $compile){
        return {
            restrict: 'E',
            replace: true,
            scope: {
                field: '=',
                ticket: '=',
                parent: '='
            },
            controller: function($scope){

            },
            link: function (scope, element) {
                $templateRequest('views/srd/fields/asteros-composite-field-directive.html').then(function(html){
                    var template = angular.element(html);
                    element.append(template);
                    $compile(template)(scope);
                });
            }
        }
    }])
})();