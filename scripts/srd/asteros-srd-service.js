(function(){
    angular.module('asterosModule').service('astSRDService', ['$resource',function($resource){
        var resource = $resource('',
            {},
            {
                questionConfigTree: {url: '/ux/rest/question_config/tree/:id', method: 'GET', isArray: true},
                instruction: {url: '/ux/rest/question_config/instruction', method: 'GET'}
            });

        this.getConfigForQuestionTreeByTicketId = function(id){
            return resource.questionConfigTree({id : id}).$promise.then(function(response){
                return response;
            })
        };

        this.getInstruction = function(value){
            return resource.instruction({value : value}).$promise.then(function(response){
                return response;
            })
        }
    }])
})();