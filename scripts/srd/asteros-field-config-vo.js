function AstFieldConfigVO(){
    this.instanceId = "";
    this.questionId = "";
    this.filterList = [];
    this.fieldList = [];
    this.keyField = null;
    this.labelField = null;
    this.isMultipleChoise = false;
    this.children = null;
    this.parent = null;
    this.format = null;
    this.maxItems = 0;
    this.relatedQuestionIds = [];
}

AstFieldConfigVO.prototype = new AstBaseVO();

AstFieldConfigVO.prototype.constructor = AstFieldConfigVO;

AstFieldConfigVO.prototype.getProps = function(){
    return ['instanceId', 'questionId', 'format', 'maxItems', 'relatedQuestionIds'];
};

AstFieldConfigVO.prototype.setFilterList = function(filterList){
    this.filterList = _.map(filterList, function(item){
        return new AstSearchConfigFieldVO().build(item);
    })
};

AstFieldConfigVO.prototype.setFieldList = function(fieldList){
    this.fieldList = _.map(fieldList, function(item){
        return new AstSearchConfigFieldVO().build(item);
    })
};

AstFieldConfigVO.prototype.setChildren = function(children){
    var self = this;
    this.children = children;
    _.forEach(children, function(child){
        child.parent = self;
    });
};

AstFieldConfigVO.prototype.postBuild = function(source){
    if (source.hasOwnProperty('filterList')){
        this.setFilterList(source.filterList);
    }
    if (source.hasOwnProperty('fieldList')){
        this.setFieldList(source.fieldList);
    }
    if (source.hasOwnProperty('keyField')){
        this.keyField = new AstSearchConfigFieldVO().build(source.keyField);
    }
    if (source.hasOwnProperty('labelField')){
        this.labelField = new AstSearchConfigFieldVO().build(source.labelField);
    }
    if (source.hasOwnProperty('isMultipleChoise') && source.isMultipleChoise === '0'){
        this.isMultipleChoise = true;
    }
};
