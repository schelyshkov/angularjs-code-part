(function () {
    angular.module('asterosModule').service('astSRDModel', ['astSRDService', '$modal', function (astSRDService, $modal) {
        this.getSRDConfigsTree = function (ticket) {
            return astSRDService.getConfigForQuestionTreeByTicketId(ticket.id).then(function (response) {
                function mapper(json) {
                    var config = new AstFieldConfigVO().build(json);
                    if (json.children && _.isArray(json.children)) {
                        var children = _.map(json.children, mapper);
                        config.setChildren(children);
                    }
                    return config;
                }

                return _.map(response, function (item) {
                    return mapper(item);
                });
            })
        };

        this.getInstruction = function(value){
            return astSRDService.getInstruction(value).then(function(response){
                if (response) {
                    return response.instruction;
                } else {
                    return "";
                }
            })
        };

        this.mapQuestionsWithConfigs = function (questions, configs) {
            function crawler(configs) {
                _.forEach(configs, function (config) {
                    var question = _.find(questions, function (item) {
                        return item.id === config.questionId;
                    });
                    if (question) {
                        config.question = question; //????????
                        config.format = question.format;
                        config.defaultLabel = question.label;
                        if (config.isMultipleChoise){
                            config.format = 14;
                        }
                        question.astIsHidden = true;
                        if (!config.parent) {
                            question.astIsHidden = false;
                            question.astField = new AstFieldVO().build({config: config})
                        }
                        if (config.children) {
                            crawler(config.children);
                        }
                    }
                });
            }

            crawler(configs);
        };

        this.getAdditionalQuery = function (config, questions, parentValue) {
            var additionalFilter = _.chain(questions)
                .filter(function (question) {
                    return question.answer && question.id !== config.question.id;
                })
                .map(function (question) {
                    return question.id + "=" + question.answer;
                }).value();
            if (parentValue && config.parent) {
                additionalFilter.push(config.parent.questionId + '=' + parentValue);
            }
            return additionalFilter;
        };

        this.showSearchModel = function (searchConfig, additionalQuery, applyHandler) {
            return $modal.open({
                templateUrl: 'views/srd/multisearch-field-modal.html',
                controller: 'AsterosMultisearchModalController',
                windowClass: 'multisearch-field__modal-wrapper',
                resolve: {
                    applyHandler: applyHandler,
                    searchConfig: function(){ return searchConfig},
                    additionalFilter: function(){ return {filter: additionalQuery}}
                }
            });
        };

        this.formatFieldByData = function (data, field) {
            data = _.filter(data, function(item){ return item.id});
            var config = field.config;
            if (!field.value) {
                field.value = [];
            }
            if (!config.children || !config.children.length) {
                field.value = _.chain(data)
                    .map(function (item) {
                        return new AstFieldValueVO().build({
                            value: item.id,
                            label: item.label,
                            questionId: field.config.questionId
                        });
                    })
                    .concat(field.value)
                    .value();
                //if (config.format !== 14 && _.isArray(field.value)) {
                if (!config.isMultipleChoise && _.isArray(field.value)) {
                    field.value = field.value.pop();
                }
                return field;
            }


            field.subFields = _.chain(data)
                .map(function (item) {
                    var childFields = _.map(config.children, function (childConfig) {
                        return new AstFieldVO().build({config: childConfig});
                    });
                    var subConfig = new AstFieldConfigVO().build({format: 15});
                    var subValue = new AstFieldValueVO().build({
                        value: item.id,
                        label: item.label,
                        questionId: field.config.questionId
                    });
                    return new AstFieldVO().build({
                        subFields: childFields,
                        config: subConfig,
                        parentField: field,
                        value: subValue
                    });
                })
                .concat(field.subFields)
                .value();


            return field;
        };

        this.flushSRDAnswers = function (answersFields) {
            function crawler(answersFields) {
                return _.chain(answersFields)
                    .map(function (field) {
                        var value = field.value;
                        if (!value || _.isArray(value) && !value.length) {
                            value = new AstFieldValueVO().build({questionId : field.config.question.id, value: ''});
                        }
                        if (field.subFields && _.isArray(field.subFields) && field.subFields.length) {
                            if (field.config.children && field.config.children.length){
                                value.questionId = null;
                            }
                            value.items = crawler(field.subFields);
                            console.log(value);
                        }
                        return value;
                    }).reduce(function (result, item) {
                        if (!item.questionId && item.items) {
                            return result.concat(item.items);
                        }
                        else if (item.questionId) {
                            result.push(item);
                            return result;
                        } else if (_.isArray(item)) {
                            return result.concat(item);
                        } else {
                            return result;
                        }
                    }, []).value();
            }

            return crawler(answersFields);
        };

        this.buildFieldsByAnswers = function (configs, answers) {
            var self = this;
            function crawler(parent, answers) {
                var data = _.map(answers, function(answer){
                    return {id: answer.value, label: answer.label};
                });
                parent = self.formatFieldByData(data, parent);
                if (parent.subFields && parent.subFields.length){
                    parent.subFields = _.map(parent.subFields, function(field, index){
                        if (field.subFields && field.subFields.length){
                            field.subFields = _.map(field.subFields, function(childSubfield){
                                var subAnswers = _.filter(answers[index].items, function(item){
                                    return item.questionId === childSubfield.config.questionId;
                                });
                                return crawler(childSubfield, subAnswers);
                            })
                        }
                        return field;
                    });
                }
                return parent;
            }

            return _.map(configs, function(config){
                var field = new AstFieldVO().build({config: config});
                var subAnswers = _.filter(answers, function(item){
                    return item.questionId === config.questionId;
                });
                return crawler(field, subAnswers);
            });
        };

        this.mapFieldsWithQuestions = function(fields, questions){
            _.forEach(fields, function(field){
                var question = _.find(questions, function(question){
                    return question.id === field.config.questionId;
                });
                if (question){
                    question.astField = field;
                }
            });
        };

        this.setValidateToFields = function(fields){
            function crawler(field){
                field.validate = true;
                if (field.subFields && field.subFields.length > 0){
                    _.forEach(field.subFields, crawler);
                }
            }

            _.forEach(fields, crawler);
        };

        this.buildAnswersByFields = function(fieldAnswers){
            var self = this;
            function crawler(fieldAnswers, parentValue){
                return _.reduce(fieldAnswers, function(result, fieldAnswer){
                    var item = {questionId: fieldAnswer.questionId};
                    var value = fieldAnswer.value;
                    if (parentValue){
                        item.value = parentValue + ':' + (value ? value : '');
                    } else{
                        item.value = value;
                    }
                    result.push(item);
                    if (fieldAnswer.items && fieldAnswer.items.length > 0){
                        result = result.concat(crawler(fieldAnswer.items, item.value));
                    }
                    return result;
                }, []);
            }

            return crawler(fieldAnswers);
        }
    }]);
})();