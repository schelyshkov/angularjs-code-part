function AstFieldValueVO(){
    this.value = null;
    this.label = null;
    this.questionId = null;
    this.items = null;
}

AstFieldValueVO.prototype = new AstBaseVO();

AstFieldValueVO.prototype.constructor = AstFieldValueVO;

AstFieldValueVO.prototype.getProps = function(){
    return ['value', 'label', 'questionId', 'items'];
};

AstFieldValueVO.prototype.postBuild = function(source){
    if (this.items && this.items.length > 0){
        this.items = _.map(this.items, function(item){
            return new AstFieldValueVO().build(item);
        })
    }
};