function AstFieldVO(){
    this.config = null;
    this.value = null;
    this.question = null;
    this.subFields = [];
    this.parentField = null;
    this.id = 'FIELD' + _.random(0, 10000, false);
}

AstFieldVO.prototype = new AstBaseVO();

AstFieldVO.prototype.constructor = AstFieldVO;

AstFieldVO.prototype.getProps = function(){
    return ['config', 'value', 'question', 'subFields', 'parentField'];
};
