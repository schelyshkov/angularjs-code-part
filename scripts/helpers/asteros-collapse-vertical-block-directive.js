(function(){
    angular.module('asterosModule').directive('asterosCollapseVerticalBlock', ['$timeout', 'i18nService', function($timeout, i18nService){
        return {
            restrict: 'A',
            link: function(scope, element, attr){
                var maxHeight = +attr["asterosCollapseVerticalBlockMaxHeight"] || 100;
                var dataField = attr["asterosCollapseVerticalBlock"];
                var isCollapseble = false;
                var isClosed = false;
                var height = -1;
                var closeBtn = angular.element('<a></a>');
                closeBtn.css('margin-top', '10px');
                closeBtn.css('display', 'inline-block');
                closeBtn.css('cursor', 'pointer');

                function close(){
                    element.css('height', maxHeight + 'px');
                    closeBtn.html(i18nService.getLocalizedString('asteros.collapse.btn.show.data') + '&nbsp;<i class="icon-angle_down"></i>');
                    isClosed = true;
                }

                function open(){
                    element.css('height', height + 'px');
                    closeBtn.html(i18nService.getLocalizedString('asteros.collapse.btn.hide.data') + '&nbsp;<i class="icon-angle_up"></i>');
                    isClosed = false;
                }

                var dataFieldWatcher = scope.$watch(dataField, function(){
                    if (isCollapseble){
                        open();
                        removeCollapseble();
                    }
                    check();
                });

                function check() {
                    $timeout(checkHeight);
                }

                function checkHeight(){
                    var newHeight = element.height();
                    height = newHeight + 20;
                    if (newHeight > maxHeight){
                        addCollapsable();
                        open();
                    }
                }

                function addCollapsable(){
                    isCollapseble = true;
                    element.css('overflow-y', 'hidden');
                    element.css('display', 'block');
                    closeBtn.on('click', function(){
                        isClosed ? open() : close();
                    });
                    closeBtn.insertAfter(element);
                }

                function removeCollapseble(){
                    isCollapseble = false;
                    //element.css('overflow-y', 'auto');
                    element.removeAttr('style');
                    closeBtn.remove();
                }

                scope.$on('$destroy', function(){
                    dataFieldWatcher();
                })
            }
        }
    }])
})();