(function () {
    angular.module('asterosModule')
        .directive('asterosDynamicSearch', [
            function () {
                return {
                    restrict: 'E',
                    replace: true,
                    templateUrl: 'views/helpers/dynamic-search-field-directive.html',
                    scope: {
                        fieldInput: '=',
                        listGetter: '=',
                        listTemplate: '@',
                        fieldLabel: '@',
                        inputPlaceholder: '@'
                    },
                    link: function (scope, element) {
                        console.log(scope);
                        scope.models = [];
                        scope.input = "";
                        scope.isLoading = false;

                        scope.$watch('models', function(){
                            var dropdownEl = element.find('.asteros-dynamic-field-dropdown');
                            var inputEl = element.find('.asteros-dynamic-field-input');
                            dropdownEl.width(inputEl.outerWidth());
                        });


                        scope.refresh = function () {
                            scope.models = [];
                            scope.fieldInput = {};
                            if (scope.input.length > 2) {
                                scope.isLoading = true;
                                scope.listGetter(scope.input).then(function (items) {
                                    scope.models = items;
                                }).finally(function(){
                                    scope.isLoading = false;
                                })
                            }
                        };

                        scope.selectItem = function (item) {
                            scope.models = [];
                            scope.fieldInput = item;
                            scope.input = scope.$eval(scope.fieldLabel, item);
                            console.log(item);
                        };


                        element.on('click', function(){
                            scope.models = [];
                            scope.isLoading = false;
                        });
                    }
                }
            }])
        .directive('asterosDynamicFieldDropdownItem', ['$templateRequest', '$compile',
            function ($templateRequest, $compile) {
                return {
                    restrict: 'E',
                    replace: true,
                    template: '<div class="asteros-dynamic-field-dropdown-item" ng-click="selectCallback()"></div>',
                    scope: {
                        'templateUrl': '@',
                        'model': '=',
                        'selectCallback': '&'
                    },
                    link: function (scope, element) {
                        $templateRequest(scope.templateUrl).then(function (html) {
                            var el = angular.element(html);
                            //el.html(html);
                            $compile(el.contents())(scope);
                            element.empty();
                            element.append(el);
                        });
                    }
                }
            }])
})();