function AstBaseVO(){

}

AstBaseVO.prototype.getProps = function(){
    return [];
};

AstBaseVO.prototype.postBuild = function(source){

};

AstBaseVO.prototype.build = function(source){
    var fields = this.getProps();
    for (var i = 0; i < fields.length; i++){
        var field = fields[i];
        if (source.hasOwnProperty(field)){
            this[field] = source[field];
        }
    }
    this.postBuild(source);
    return this;
};