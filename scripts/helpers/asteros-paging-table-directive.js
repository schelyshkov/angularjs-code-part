(function () {
    angular.module('asterosModule').directive('asterosPagingTable', [function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'views/helpers/paging-table-directive.html',
            scope: {
                config: '=',
                records: '=',
                onSelect: '&',
                isSelectable: '=',
                page: '@',
                pageSize: '@'
            },
            link: function (scope) {
                scope.page = scope.page || 1;
                scope.pageSize = scope.pageSize || 10;
                scope.pageNumbersList = [];
                scope.records = scope.records || [];
                scope.config = scope.config || {labels: [], fields: []};
                scope.labels = [];
                scope.fields = [];
                scope.dataType = '';
                scope.pageItems = [];
                scope.items = [];

                scope.$watch('records', function(newVal){
                    scope.items = newVal.map(function(record){
                        return {
                            isSelected: false,
                            data: record
                        }
                    });
                    scope.moveToPage(1);
                });

                scope.$watch('config', function(newVal){
                    scope.labels = newVal.labels;
                    scope.fields = newVal.fields;
                });

                scope.select = function(item){
                    item.isSelected = !item.isSelected;
                    scope.onSelect({item: item.data, isSelected: item.isSelected});
                };

                scope.moveToPage = function(page){
                    scope.pageItems = scope.items.slice((page - 1) * scope.pageSize, page * scope.pageSize);
                    scope.page = page;
                    setPageNumbersList();
                };

                scope.getLastPageNumber = function(){
                    return Math.ceil(scope.items.length / scope.pageSize);
                };

                function setPageNumbersList(){
                    var lastPage = scope.getLastPageNumber();
                    var currentPage = scope.page;
                    var startPage = currentPage - 5 > 0 ? currentPage - 5 : 1;
                    var pageNumbers = [];
                    for (var i = startPage; i <= startPage + 10 && i <= lastPage; i++){
                        pageNumbers.push(i);
                    }
                    scope.pageNumbersList = pageNumbers;
                }
            }
        }
    }])
})();