(function(){
    angular.module('asterosModule').controller('AstChangeCalendarController', ['$scope', '$state', 'events', 'ticketService',
        function($scope, $state, events, ticketService){
            $scope.draftTicket = {
                type: EntityVO.TYPE_CHANGE
            };
            $scope.data = {};
            $scope.data.searches = [];

            function init(){
                $scope.draftTicket = {
                    type: EntityVO.TYPE_CHANGE,
                    scheduledStartDatePicker: { open: false },
                    scheduledEndDatePicker: { open: false },
                    actualStartDatePicker: { open: false },
                    actualEndDatePicker: { open: false },
                    targetDatePicker: { open: false },
                    customFields: {},
                    needsReloadingRiskQuestions: true
                };
                ticketService.getDraft(EntityVO.TYPE_CHANGE).then(function(response){
                    $scope.draftTicket = angular.extend($scope.draftTicket, response);
                });
                if ($state.current.name === 'asterosShowChangeCalendar'){
                    $state.go('asterosShowChangeCalendar.calendar.book');
                }
            }
            init();

            function getLinkedCIs() {
                var linkedCIs = [],
                    bulkCIsLinked = [];
                _.forEach($scope.data.searches, function (search) {
                    _.forEach(search.results, function (item) {
                        _.forEach(item.relations, function (relation) {
                            if (search.allQueryItemsRelation && search.allQueryItemsRelation.length > 0) {
                                if (_.findWhere(search.allQueryItemsRelation, { relationshipType: relation })) {
                                    return;
                                }
                            }
                            linkedCIs.push({
                                relationshipType: relation,
                                tag: EntityVO.TYPE_LINKEDITEM,
                                id: item.reconciliationId,
                                type: EntityVO.TYPE_ASSET,
                                desc: item.name
                            });
                        });
                    });
                });

                return { linkedItems: linkedCIs, bulkCIsLinked: bulkCIsLinked };
            }

            $scope.$on(events.LINKED_COUNT, function (e, searches) {
                $scope.draftTicket.linkedCIs = getLinkedCIs().linkedItems;
            });

            $scope.$watch('draftTicket.scheduledStartDate', function(){
                $scope.draftTicket.linkedCIs = getLinkedCIs().linkedItems;
            });

            $scope.$watch('draftTicket.scheduledEndDate', function(){
                $scope.draftTicket.linkedCIs = getLinkedCIs().linkedItems;
            });
        }])
})();