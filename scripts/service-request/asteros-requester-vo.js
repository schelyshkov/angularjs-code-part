function AstRequesterVO(){
    this.fullName = "";
    this.loginId = "";
    this.ticketId = "";
    this.corporateId = "";
    this.jobTitle = "";
    this.location = "";
    this.displayName = "";
}

AstRequesterVO.prototype = new BaseVO();

AstRequesterVO.prototype.constructor = AstRequesterVO;

AstRequesterVO.prototype.getProps = function(){
    return ['fullName', 'loginId', 'ticketId', 'corporateId', 'jobTitle', 'location'];
};

AstRequesterVO.prototype.postBuild = function(){
    this.displayName = this.fullName + ' (' + this.loginId + ') ' + this.corporateId + ', ' + this.jobTitle + ', ' + this.location;
};