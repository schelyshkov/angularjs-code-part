(function(){
    'use strict';

    angular.module('asterosModule').service('astServiceRequestService', ['$resource',
        function($resource){
            var resource = $resource('',
                {},
                {
                    applyAction: {url : '/ux/rest/request/:id/:action', method: 'PUT'},
                    getRequesters: {url: '/ux/rest/request/requesters/:id', method: 'GET', isArray: true},
                    saveAnswers: {url: '/ux/rest/request/answers/:id', method: 'POST', isArray: true},
                    getAnswers: {url: '/ux/rest/request/answers/:id', method: 'GET', isArray: true}
                });

            this.applyAction = function(data, action, comment){
                var payload = {};
                if (comment){
                    payload.comment = comment;
                }
                return resource.applyAction({id: data.id, action: action}, payload).$promise.then(function(response){
                   return response;
                });
            };

            this.getRequesters = function(ticketId){
                return resource.getRequesters({id: ticketId}, {}).$promise.then(function(response){
                    return response;
                });
            };

            this.saveAnswers = function(srId, answers){
                return resource.saveAnswers({id : srId}, answers).$promise.then(function(response){
                    return response;
                })
            };

            this.getAnswers = function(srId){
                return resource.getAnswers({id : srId}).$promise.then(function(response){
                    return response;
                });
            };
    }])
})();