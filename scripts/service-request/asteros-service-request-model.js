(function () {
    'use strict';

    angular.module('asterosModule').service('astServiceRequestModel', ['astServiceRequestService',
        function (astServiceRequestService) {
            this.applyAction = function(serviceRequest, action, comment){
                return astServiceRequestService.applyAction(serviceRequest, action, comment).then(function(response){
                    return response;
                });
            };

            this.getRequesters = function(ticketId){
                return astServiceRequestService.getRequesters(ticketId).then(function(response){
                    var requesters = _.map(response, function(item){
                        return new AstRequesterVO().build(item);
                    });
                    return requesters;
                });
            };

            this.saveAnswers = function(srId, answers){
                return astServiceRequestService.saveAnswers(srId, answers).then(function(response){
                    return response.map(function(item){
                        return new AstFieldValueVO().build(item);
                    });
                });
            };

            this.getAnswers = function(srId){
                return astServiceRequestService.getAnswers(srId).then(function(response){
                    return response.map(function(item){
                        return new AstFieldValueVO().build(item);
                    });
                });
            };

            this.cloneAnswers = function(srId){
                function flushAnswer(answer){
                    answer.ticketId = null;
                    answer.instanceId = null;
                    answer.entryId = null;
                    if (answer.items){
                        answer.items = _.map(answer.items, flushAnswer);
                    }
                    return answer;
                }

                return this.getAnswers(srId).then(function(answers){
                    return _.map(answers, flushAnswer);
                });
            };
        }])
})();