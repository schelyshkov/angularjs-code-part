function AstApproverVO(){
    this.fullName = "";
    this.loginId = "";
    this.isWrong = "";
}

AstApproverVO.prototype = new BaseVO();

AstApproverVO.prototype.constructor = AstApproverVO;

AstApproverVO.prototype.getProps = function(){
    return ['fullName', 'loginId', 'isWrong'];
};

AstApproverVO.prototype.postBuild = function(){

};