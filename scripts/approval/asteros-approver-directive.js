(function(){
    angular.module('asterosModule').directive('asterosApprover', [function(){
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'views/approval/asteros-approver-directive.html',
            scope: {
                approver: '=',
                isEditable: '='
            },
            link: function(scope){
                scope.isEdit = false;
                scope.isDirty = false;
                scope.isValid =  !scope.approver.isWrong;

                scope.toggleEdit = function(){
                    scope.isEdit = !scope.isEdit;
                };

                scope.$on(ASTEROS_EVENTS.APPROVAL.APPROVER.SELECTED, function(event, data){
                    if (data.loginId !== scope.approver.loginId){
                        scope.approver.build(data);
                        scope.approver.isModified = true;
                        scope.approver.isWrong = false;
                        scope.isDirty = true;
                        scope.isValid = true;
                        scope.$emit(ASTEROS_EVENTS.APPROVAL.APPROVER.CHANGED);
                    }
                    scope.isEdit = false;
                    event.stopPropagation();
                })
            }
        }
    }])
})();