function AstApprovalStageVO(){
    this.ticketId = "";
    this.stage = "";
    this.approvals = [];
    this.isClosed = true;
    this.status = "";
    this.isValid = true;
}

AstApprovalStageVO.prototype = new BaseVO();

AstApprovalStageVO.prototype.constructor = AstApprovalStageVO;

AstApprovalStageVO.prototype.getProps = function(){
    return ['ticketId', 'stage', 'approvals', 'isClosed'];
};

AstApprovalStageVO.prototype.postBuild = function(){

};

AstApprovalStageVO.prototype.fill = function(approvals){
    var self = this;
    this.approvals = approvals;
    this.stage = approvals[0].stage;
    this.ticketId = approvals[0].ticketId;
    this.status = null;
    _.forEach(approvals, function(approval){
        if (_.contains([ASTEROS_APPROVAL_STATUSES.PENDING, ASTEROS_APPROVAL_STATUSES.FUTURE], approval.status)) {
            self.isClosed = false;
        }

        if (self.status !== ASTEROS_APPROVAL_STAGE_STATUSES.PREVIOUS) {
            if (_.contains([ASTEROS_APPROVAL_STATUSES.SYSTEM_PENDING], approval.status)){
                self.status = ASTEROS_APPROVAL_STAGE_STATUSES.STARTING;
            }
            if (_.contains([ASTEROS_APPROVAL_STATUSES.PENDING], approval.status)) {
                self.status = ASTEROS_APPROVAL_STAGE_STATUSES.CURRENT;
            }
            if (_.contains([ASTEROS_APPROVAL_STATUSES.FUTURE], approval.status)) {
                self.status = ASTEROS_APPROVAL_STAGE_STATUSES.NEXT;
            }
        }
        if (_.contains([ASTEROS_APPROVAL_STATUSES.REJECTED, ASTEROS_APPROVAL_STATUSES.CANCELED], approval.status)){
            self.status = ASTEROS_APPROVAL_STAGE_STATUSES.PREVIOUS;
        }
    });
    if (!this.status){
        this.status = ASTEROS_APPROVAL_STAGE_STATUSES.PREVIOUS;
    }
    //this.isValid = this.check();
    return this;
};

AstApprovalStageVO.prototype.check = function(){
  return _.reduce(this.approvals, function(result, approval){
     return result && approval.check();
  }, true);
};