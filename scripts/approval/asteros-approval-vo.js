function AstApprovalVO(){
    this.stage = "";
    this.approvers = [];
    this.ticketId = "";
    this.status = "";
    this.approvalId = "";
    this.isExpert = null;
    this.isAssistant = null;
    this.entryId = null;
}

AstApprovalVO.prototype = new BaseVO();

AstApprovalVO.prototype.constructor = AstApprovalVO;

AstApprovalVO.prototype.getProps = function(){
    return ['stage', 'approvers', 'ticketId', 'status', 'approvalId', 'isExpert', 'isAssistant', 'entryId'];
};

AstApprovalVO.prototype.postBuild = function(){
    var approvers = this.approvers;
    this.approvers = null;

    this.approvers = approvers.map(function(item){
        var approver = new AstApproverVO().build(item);
        approver.entryId = item.entryId;
        return approver;
    });
};

AstApprovalVO.prototype.check = function(){
    return _.reduce(this.approvers, function(result, approver){
        return result && !approver.isWrong;
    }, true);
};