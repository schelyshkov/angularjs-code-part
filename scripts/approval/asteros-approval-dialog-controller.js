(function () {
	'use strict';

	angular.module('asterosModule')
		.controller('AsterosApprovalDialogController', ['$scope', 'changeApprovalParams', 'astApprovalModel', 'astApprovalsContainer', '$q',
		function ($scope, changeApprovalParams, astApprovalModel, astApprovalsContainer, $q) {
            $scope.state = {
            	saving: false,
				loading: false
			};
            $scope.ticketId = changeApprovalParams.id;
            $scope.stages = angular.copy(astApprovalsContainer.approvals);
            $scope.nextStages = [];
            $scope.previousStages = [];
            $scope.accordionState = {isOpen: []};
            $scope.validStagesMap = [];
            $scope.state.loading = false;
            $scope.isDirty = false;
            checkValid();
            setOpenedTab();

            $scope.$on(ASTEROS_EVENTS.APPROVAL.CHANGED, function(event, data){
                checkValid();
                $scope.isDirty = true;
                event.stopPropagation();
            });

            function setOpenedTab(){
                $scope.accordionState.isOpen = [];
                _.forEach($scope.stages, function(stage, key){
                    if (stage.status === ASTEROS_APPROVAL_STAGE_STATUSES.CURRENT || !$scope.validStagesMap[key]){
                        $scope.accordionState.isOpen.push(true);
                    }
                    else{
                        $scope.accordionState.isOpen.push(false);
                    }
                })
            }

            function checkValid(){
                $scope.validStagesMap = [];
                _.forEach($scope.stages, function(stage){
                    $scope.validStagesMap.push(stage.check());
                })
            }

            $scope.cancel = function(){
                $scope.isDirty = false;
                $scope.$broadcast(ASTEROS_EVENTS.APPROVAL.CANCEL);
            };

            $scope.save = function(){
                $scope.state.saving = true;
                var approvals = _.reduce($scope.stages, function(result, stage){
                    var modifiedApprovals = _.filter(stage.approvals, function(approval){
                        return approval.isModified;
                    });
                    return result.concat(modifiedApprovals);
                }, []);

                var promises = _.map(approvals, astApprovalModel.update);
                $q.all(promises).then(function(responses){
                    $scope.isDirty = false;
                    _.forEach($scope.stages, function(stage, key){
                        astApprovalsContainer.approvals[key] = stage;
                    });
                    $scope.stages = angular.copy($scope.stages);
                }).finally(function(){
                    $scope.state.saving = false;
                });
            }
		}]);
})();
