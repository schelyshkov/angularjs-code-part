(function(){
    'use strict';

    angular.module('asterosModule').directive('asterosEditApproverField', ['approvalModel', function(approvalModel){
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'views/approval/asteros-edit-approver-field-directive.html',
            scope: {
                approver: '='
            },
            link: function(scope){
                scope.state = {
                    loadingApproverSearch: false
                };

                scope.getDisplayData = function(user){
                    return user.fullName;
                };
                scope.input = scope.getDisplayData(scope.approver);

                scope.getApproverByText = function (searchText) {
                    return approvalModel.getApproverByText(searchText);
                };

                scope.update = function(item){
                    scope.input = scope.getDisplayData(item);
                    var approver = new AstApproverVO().build(item);
                    scope.$emit(ASTEROS_EVENTS.APPROVAL.APPROVER.SELECTED, approver);
                };
                scope.cancel = function(){
                    console.log(scope.approver);
                    scope.$emit(ASTEROS_EVENTS.APPROVAL.APPROVER.SELECTED, scope.approver);
                }
            }
        }
    }]);
})();