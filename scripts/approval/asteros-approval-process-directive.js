(function(){
   'use strict';

   angular.module('asterosModule')
       .directive('asterosApprovalProcess', ['astApprovalModel', function(astApprovalModel){
           return {
               restrict: 'E',
               scope: {
                   ticket: '='
               },
               replace: true,
               templateUrl: 'views/approval/asteros-approval-process-directive.html',
               link: function(scope){
                   scope.stages = [];
                   scope.dataSaving = false;

                   astApprovalModel.getTicketApprovals({ticketId: scope.ticket.id}).then(function(approvals){
                       var stages = {};
                       _.forEach(approvals, function(approval){
                           if (!stages[approval.stage]) stages[approval.stage] = [];
                          stages[approval.stage].push(approval);
                       });
                       console.log(stages);

                       _.forEach(stages, function(stage){
                          scope.stages.push(new AstApprovalStageVO().fill(stage));
                       });
                   });

                   scope.$on(ASTEROS_EVENTS.APPROVAL.SAVING, function(event, data){
                       //console.log('asterosApprovalProcess $on(ASTEROS_EVENT_APPROVAL_SAVING)', data)
                       scope.dataSaving = true;
                       event.stopPropagation();
                   });

                   scope.$on(ASTEROS_EVENTS.APPROVAL.SAVED, function(event, data){
                       //console.log('asterosApprovalProcess $on(ASTEROS_EVENT_APPROVAL_SAVED)', data)
                       scope.dataSaving = false;
                       event.stopPropagation();
                   });
               }
           }
       }])
})();