(function () {
    angular.module('asterosModule').controller('AsterosApprovalCommentDialogController', ['$scope', 'astParams',
        function ($scope, astParams) {
            $scope.comment = '';
            $scope.state = {saving: false};

            $scope.apply = function(){
                $scope.state = true;
                astParams.callback($scope.comment).then(function(){
                    $scope.state = false;
                    $scope.$dismiss();
                });
            }
        }])
})();