(function () {
    'use strict';

    angular.module('asterosModule').directive('asterosApproval', ['astApprovalModel', 'i18nService', 'userModel',
        function (astApprovalModel, i18nService, userModel) {
            return {
                restrict: 'E',
                scope: {
                    approval: '='
                },
                replace: true,
                templateUrl: 'views/approval/asteros-approval-directive.html',
                link: function (scope) {
                    scope.oldApprovers = angular.copy(scope.approval.approvers);
                    setApprovalType(scope.approval);
                    setIsEditable(scope.approval);

                    scope.getStatusClass = function (status) {
                        switch (status) {
                            case ASTEROS_APPROVAL_STATUSES.PENDING:
                                return 'icon-pending';
                            case ASTEROS_APPROVAL_STATUSES.REJECTED:
                                return 'icon-rejected';
                            case ASTEROS_APPROVAL_STATUSES.APPROVED:
                                return 'icon-approved';
                            case ASTEROS_APPROVAL_STATUSES.CANCELED:
                                return 'icon-redo';
                            case ASTEROS_APPROVAL_STATUSES.FUTURE:
                                return 'icon-arrow_right_brackets';
                            default:
                                return 'icon-pending';
                        }
                    };

                    scope.$on(ASTEROS_EVENTS.APPROVAL.APPROVER.CHANGED, function (event) {
                        scope.approval.isModified = true;
                        scope.$emit(ASTEROS_EVENTS.APPROVAL.CHANGED);
                    });

                    scope.$on(ASTEROS_EVENTS.APPROVAL.CANCEL, function(event){
                        scope.approval.isModified = false;
                        scope.approval.approvers = angular.copy(scope.oldApprovers);
                    });

                    function setApprovalType(approval) {
                        if (approval.isExpert) {
                            scope.approvalType = i18nService.getLocalizedString('asteros.approval.type.expert');
                        } else if (approval.isAssistant){
                            scope.approvalType = i18nService.getLocalizedString('asteros.approval.type.assistant');
                        }
                    }
                    function setIsEditable(approval){
                        scope.isEditable = false;
                        userModel.getFullCurrentUserData().then(function(userData){
                            scope.isEditable = _.contains([ASTEROS_APPROVAL_STATUSES.FUTURE, ASTEROS_APPROVAL_STATUSES.PENDING], approval.status);
                            scope.isEditable = scope.isEditable && (userData.supportGroups.filter(function(item){ return item.name === ASTEROS_APPROVAL_PERMISSION_GROUP}).length > 0);
                        });
                    }
                }
            }
        }])
})();