(function(){
    'use strict';

    angular.module('asterosModule').directive('asterosApprovalStage', [function(){
        return {
            restrict: 'E',
            scope: {
                stage: '='
            },
            replace: true,
            templateUrl: 'views/approval/asteros-approval-stage-directive.html',
            link: function(scope){
            }
        }
    }])
})();