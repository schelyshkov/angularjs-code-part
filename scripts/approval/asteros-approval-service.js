(function () {
    'use strict';

    angular.module('asterosModule').service('astApprovalService', ['$resource', '$q', '$timeout', '$modal',
        function ($resource, $q, $timeout, $modal) {
            var resource = $resource('',
                {},
                {
                    getTicketApprovals: {url: '/ux/rest/approval/list/:id', method: 'GET', isArray: true},
                    updateApproval: {url: '/ux/rest/approval/:approvalId', method: 'PUT'}
                });

            this.getTicketApprovals = function (data) {
                return resource.getTicketApprovals({id: data.ticketId}).$promise.then(function (response) {
                    return response;
                });
            };

            this.updateApproval = function(approvalId, data){
                return resource.updateApproval({approvalId: approvalId},data).$promise.then(function(response){
                    return response;
                });
            };

            this.showApproversDialog = function (context, approvals) {
                return $modal.open({
                    templateUrl: 'views/approval/asteros-approval-dialog-controller.html',
                    windowClass: 'action-blade',
                    controller: 'AsterosApprovalDialogController',
                    resolve: {
                        changeApprovalParams: function () {
                            return {
                                id: context && context.id,
                                type: context && context.type,
                                statusValue: context && context.status.value,
                                addApproverAllowed: context && context.accessMappings && context.accessMappings.addApproverEditAllowed,
                            };
                        },
                        astApprovalsContainer: function(){
                            return {
                                approvals: approvals
                            }
                        }
                    }
                });
            };

            this.showCommentDialog = function(action, callback){
                return $modal.open({
                    templateUrl: 'views/approval/asteros-approval-comment-dialog-controller.html',
                    windowClass: 'action-blade',
                    controller: 'AsterosApprovalCommentDialogController',
                    resolve: {
                        astParams: function () {
                            return {
                                action: action,
                                callback: callback
                            };
                        }
                    }
                });
            }
        }])
})();