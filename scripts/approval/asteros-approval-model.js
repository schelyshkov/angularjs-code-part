(function(){
    'use strict';

    angular.module('asterosModule').service('astApprovalModel', ['astApprovalService', function(astApprovalService){
        var self = this;

        var parseApprovalsResponse = function(response){
            var approvals = [];
            if (response[0].items) {
                _.forEach(response[0].items, function (item) {
                    approvals.push(new AstApprovalVO().build(item))
                });
            }
            return approvals;
        };

        this.getTicketApprovals = function(data){
            return astApprovalService.getTicketApprovals(data).then(function(response){
                return parseApprovalsResponse(response);
            });
        };

        this.getApproversListByCompany = function (data, company) {
            return astApprovalService.getApproversListByCompany(data, company).then(function (response) {
                return response[0].items;
            });
        };

        this.update = function(data){
            return astApprovalService.updateApproval(data.entryId, data).then(function(response){
                return new AstApprovalVO().build(response);
            });
        };

        this.checkApprovalStages = function(stages){
            return _.reduce(stages, function(result, stage){
                return result && stage.check();
            }, true);
        }
    }])
})();