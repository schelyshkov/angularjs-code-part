var ASTEROS_APPROVAL_STATUSES = {
    PENDING: 1,
    FUTURE: 0,
    APPROVED: 2,
    REJECTED: 3,
    CANCELED: 4,
    SYSTEM_PENDING: 10000
};

var ASTEROS_APPROVAL_STAGE_STATUSES = {
    PREVIOUS: "previous",
    CURRENT: "current",
    NEXT: "next",
    STARTING: "starting"
};

var ASTEROS_APPROVAL_PERMISSION_GROUP = 'Секретариат';

var ASTEROS_APPROVER_PROFILE_STATUSES = {
    VALID: '1'
};

var ASTEROS_TICKET_ACTIONS = {
    NEXT: 'next',
    BACK: 'back'
};

var ASTEROS_EVENTS = {
    APPROVAL: {
        CHANGED: 'ASTEROS_EVENT_APPROVAL_CHANGED',
        SAVING: 'ASTEROS_EVENT_APPROVAL_SAVING',
        SAVED: 'ASTEROS_EVENT_APPROVAL_SAVED',
        CANCEL: 'ASTEROS_EVENT_APPROVAL_CANCEL',
        APPROVER: {
            CHANGE: 'ASTEROS_EVENT_APPROVAL_APPROVER_CHANGE',
            CHANGED: 'ASTEROS_EVENT_APPROVAL_APPROVER_CHANGED',
            SELECTED: 'ASTEROS_EVENT_APPROVER_APPROVER_SELECTED'
        }
    },
    SRD: {
        CHANGED: 'ASTEROS_EVENT_SRD_CHANGED',
        MULTICHOISE_REFRESH: 'ASTEROS_EVENT_SRD_MULTICHOISE_REFRESH'
    }
};

var ASTEROS_POSSIBLE_STATUSES = {
    REQUEST: ['Waiting Approval', 'Planning', 'Pending', 'Cancelled', 'Rejected', 'Completed', 'Closed'],
    WORKORDER: ['Assigned', 'In Progress', 'Pending', 'Cancelled', 'Closed']
};